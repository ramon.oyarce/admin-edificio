package com.admin.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages={"com.admin"})
public class AdminEdificioApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminEdificioApplication.class, args);
	}

}
